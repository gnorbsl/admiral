package admiral.patric.mamolapps.de.admiral.helper;

import com.unnamed.b.atv.model.TreeNode;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import admiral.patric.mamolapps.de.admiral.R;

/**
 * Created by dramalama on 13.05.2015.
 */
public class CategoryTreeHolder
        extends TreeNode.BaseNodeViewHolder<CategoryTreeHolder.CategoryItem> {


    private TextView tvValue;

    public CategoryTreeHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(TreeNode treeNode, CategoryItem categoryItem) {

        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_category_node, null, false);
        tvValue = (TextView) view.findViewById(R.id.node_value);
        tvValue.setText(categoryItem.text);

        final ImageView iconView = (ImageView) view.findViewById(R.id.category_icon);
        iconView.setImageResource(categoryItem.icon);

        return view;
    }

    public static class CategoryItem {

        public int icon;

        public String text;


        public CategoryItem(int icon, String text) {
            this.icon = icon;
            this.text = text;

        }
    }
}
