package admiral.patric.mamolapps.de.admiral.fragments;

import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import admiral.patric.mamolapps.de.admiral.R;
import admiral.patric.mamolapps.de.admiral.helper.CategoryTreeHolder;
import admiral.patric.mamolapps.de.admiral.helper.Constants;
import admiral.patric.mamolapps.de.admiral.helper.LanguageTreeHolder;
import admiral.patric.mamolapps.de.admiral.helper.SoundTreeHolder;


public class SoundsListFragment extends Fragment {

    private MenuItem flagItem;

    public static SoundsListFragment newInstance() {

        SoundsListFragment fragment = new SoundsListFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //show the "choose flag" menuitem
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sounds, container, false);

        ViewGroup treeViewGroup = (ViewGroup) view.findViewById(R.id.test);

        generateTreeList(treeViewGroup);

        return view;
    }

    /**
     * Generates the different items and subitems of the TreeView
     */
    private void generateTreeList(final ViewGroup treeViewGroup) {

        TreeNode root = TreeNode.root();

        TreeNode ownSoundCategory = createCategoryListItem(R.drawable.ic_kat_eigenetoene,
                "Eigene Sounds");

        TreeNode gargamel = createCategoryChild(R.raw.gargamel, "Gargamel");
        TreeNode luschtig = createCategoryChild(R.raw.luschtig, "Luschtig");

        TreeNode[] languageDummies = createLanguageDummies();

        luschtig.addChildren(languageDummies);

        ownSoundCategory.addChildren(gargamel, luschtig);

        TreeNode niceSoundCategory = createCategoryListItem(R.drawable.ic_kat_nett,
                "Kategorie Nett");
        TreeNode usefulSoundCategory = createCategoryListItem(R.drawable.ic_kat_nuetzlich,
                "Kategorie Nützlich");
        TreeNode angrySoundCategory = createCategoryListItem(R.drawable.ic_kat_sauer,
                "Kategorie Sauer");
        TreeNode funSoundCategory = createCategoryListItem(R.drawable.ic_kat_spass,
                "Kategorie Spass");
        TreeNode wiseSoundCategroy = createCategoryListItem(R.drawable.ic_kat_weise,
                "Kategorie Weise");

        root.addChildren(ownSoundCategory,
                niceSoundCategory,
                usefulSoundCategory,
                angrySoundCategory,
                funSoundCategory,
                wiseSoundCategroy);

        AndroidTreeView treeView = new AndroidTreeView(getActivity(), root);
        treeView.setDefaultAnimation(true);
        treeView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom);

        treeViewGroup.addView(treeView.getView());

    }

    /**
     * @param soundId    raw resource id of the soundfile
     * @param soundTitle name of the sound
     * @return Categorysubitem
     */
    private TreeNode createCategoryChild(int soundId, String soundTitle) {

        return new TreeNode(
                new SoundTreeHolder.SoundItem(soundTitle, soundId)).setViewHolder(
                new SoundTreeHolder(getActivity()));

    }

    /**
     * @param iconId      Drawable resource id of the categoryicon
     * @param description Name of the category
     * @return TreeNode Item
     */
    private TreeNode createCategoryListItem(int iconId, String description) {

        return new TreeNode(
                new CategoryTreeHolder.CategoryItem(iconId,
                        description)).setViewHolder(new CategoryTreeHolder(getActivity()));
    }

    //TODO this is just ugly as hell remove it ASAP
    private TreeNode[] createLanguageDummies() {

        String[] languages = getResources().getStringArray(R.array.language_options);
        TreeNode[] languageDummies = new TreeNode[languages.length];

        for (int i = 0; i < languages.length; i++) {

            languageDummies[i] = new TreeNode(new LanguageTreeHolder.LanguageItem(languages[i]))
                    .setViewHolder(new LanguageTreeHolder(getActivity()));

        }

        return languageDummies;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        flagItem = menu.add(R.string.change_language);

        flagItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        //get stored default language from preferences
        SharedPreferences preferences = getSharedPreferences();
        int flagResource = preferences.getInt(Constants.FLAG_RESOURCE_KEY, R.drawable.flag_0);

        flagItem.setIcon(flagResource);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // return super.onOptionsItemSelected(item);

        if (item.getTitle().equals(getResources().getString(R.string.change_language))) {

            startChooseLanguageDialog();

        }

        return true;

    }

    /**
     * Displays a dialog to change the default soundlanguage
     */
    private void startChooseLanguageDialog() {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("languageDialog");

        //remove previous dialogs before showing a new one
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = new LanguageDialogFragment();
        newFragment.setTargetFragment(this, 0);
        newFragment.show(ft, "languageDialog");

    }

    /**
     * changes the flag of the menu item
     *
     * @param position which position was chosen in the choose language dialog ->
     *                 'startChooseLanguageDialog()'
     */
    public void changeFlag(int position) {

        if (flagItem != null) {

            int flagId = getResources()
                    .getIdentifier("flag_" + position, "drawable", getActivity().getPackageName());

            Drawable flag = getResources().getDrawable(flagId);

            flagItem.setIcon(flag);

            storeCurrentFlag(flagId);
            storeCurrentLanguage(position);


        }

    }

    /**
     * stores the chosen anguage in the database
     *
     * @param position which position was chosen in the choose language dialog ->
     *                 'startChooseLanguageDialog()'
     */
    private void storeCurrentLanguage(int position) {

        SharedPreferences preferences = getSharedPreferences();

        preferences
                .edit()
                .putInt(Constants.CURRENT_LANGUAGE_STRING_KEY, position)
                .apply();

    }

    /**
     * stores the drawable resource id of the flag image in the database
     *
     * @param flagId drawable resource id of the current flag image
     */
    private void storeCurrentFlag(int flagId) {

        SharedPreferences preferences = getSharedPreferences();

        preferences
                .edit()
                .putInt(Constants.FLAG_RESOURCE_KEY, flagId)
                .apply();

    }


    private SharedPreferences getSharedPreferences() {

        return getActivity().getSharedPreferences(
                Constants.PREFERENCE_IDENTIFIER,
                Context.MODE_PRIVATE);
    }
}
