package admiral.patric.mamolapps.de.admiral.helper;

/**
 * Created by dramalama on 22.05.2015.
 */
public class Constants {

    public final static String FLAG_RESOURCE_KEY = "flagResource";

    public static final String PREFERENCE_IDENTIFIER = "AdmiralSettings";

    public static final String CURRENT_LANGUAGE_STRING_KEY = "currentLanguagePosition";

    public static boolean first = true;
}
