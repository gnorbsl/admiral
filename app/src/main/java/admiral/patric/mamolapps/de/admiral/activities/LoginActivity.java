package admiral.patric.mamolapps.de.admiral.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import admiral.patric.mamolapps.de.admiral.R;


public class LoginActivity extends Activity {

    private EditText mUsernameEditText;

    private EditText mPasswordEditText;

    private Button mLoginButton;

    private TextView mRegisterTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation hyperspaceJumpAnimation = AnimationUtils
                        .loadAnimation(LoginActivity.this, R.anim.button_click_animation);

                hyperspaceJumpAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (inputFieldsAreValidated()) {
                            startLogin();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                mLoginButton.startAnimation(hyperspaceJumpAnimation);


            }
        });

        mRegisterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(LoginActivity.this, "Registrierung gewählt", Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }


    /**
     * retreive logindata and start mainactivity, remove the LoginActivity from BackStack
     */
    private void startLogin() {

        //TODO get logindata

        Intent mainActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
        //Make sure, that the LoginActivity is removed from stack
        mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainActivityIntent);


    }

    /**
     * validate if the inputfields are not empty and username/password are correct
     *
     * @return check successful?
     */
    private boolean inputFieldsAreValidated() {

        //TODO validate edittexts
        return true;

    }

    private void initViews() {

        mUsernameEditText = (EditText) findViewById(R.id.login_edittext_username);
        mPasswordEditText = (EditText) findViewById(R.id.login_edittext_password);
        mLoginButton = (Button) findViewById(R.id.login_button_login);
        mRegisterTextView = (TextView) findViewById(R.id.login_textview_register);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
