package admiral.patric.mamolapps.de.admiral.activities;

import com.astuetz.PagerSlidingTabStrip;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import admiral.patric.mamolapps.de.admiral.R;
import admiral.patric.mamolapps.de.admiral.adapter.MainActivityViewPagerAdapter;

public class MainActivity extends ActionBarActivity {

    private ViewPager mViewPager;

    private PagerSlidingTabStrip mPagerTabs;

    private MainActivityViewPagerAdapter mMainActivityViewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        mMainActivityViewPagerAdapter = new MainActivityViewPagerAdapter(
                getSupportFragmentManager(), getTabNames());

        mViewPager.setAdapter(mMainActivityViewPagerAdapter);

        mPagerTabs.setViewPager(mViewPager);
        mPagerTabs.setShouldExpand(true);
        mPagerTabs.setIndicatorColorResource(R.color.mamol_yellow);

    }


    private String[] getTabNames() {

        return getResources().getStringArray(R.array.tabnames);

    }

    /**
     * View initialization
     */
    private void initViews() {

        mViewPager = (ViewPager) findViewById(R.id.main_viewpager);
        mPagerTabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
