package admiral.patric.mamolapps.de.admiral.helper;

import com.unnamed.b.atv.model.TreeNode;

import android.content.Context;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import admiral.patric.mamolapps.de.admiral.R;

/**
 * Created by dramalama on 13.05.2015.
 */
public class LanguageTreeHolder
        extends TreeNode.BaseNodeViewHolder<LanguageTreeHolder.LanguageItem> {


    private TextView tvValue;

    private ImageView showMore;


    public LanguageTreeHolder(Context context) {
        super(context);

    }

    @Override
    public View createNodeView(final TreeNode treeNode, final LanguageItem languageItem) {

        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_language_node, null, false);
        tvValue = (TextView) view.findViewById(R.id.language_name);

        tvValue.setText(languageItem.text);

        tvValue.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {

                Log.d("DRAG", event.getX() + " " + event.getY());

                return true;
            }
        });

        return view;
    }

    public static class LanguageItem {

        public String text;


        public LanguageItem(String text) {
            this.text = text;

        }
    }
}
