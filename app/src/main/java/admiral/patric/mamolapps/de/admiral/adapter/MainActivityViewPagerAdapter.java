package admiral.patric.mamolapps.de.admiral.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import admiral.patric.mamolapps.de.admiral.fragments.DummyFragment;
import admiral.patric.mamolapps.de.admiral.fragments.SoundsListFragment;

/**
 * Created by dramalama on 13.05.2015.
 */
public class MainActivityViewPagerAdapter extends FragmentPagerAdapter {

    private final String[] tabNames;

    public MainActivityViewPagerAdapter(FragmentManager fm, String[] tabNames) {
        super(fm);

        this.tabNames = tabNames;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return SoundsListFragment.newInstance();
           /* case 1:
                DialogFragment frag = AudioPlayerFragment.newInstance(R.raw.banana);
                frag.setShowsDialog(false);
                return frag;*/
            default:
                return DummyFragment.newInstance("test", "tet");
        }
    }

    @Override
    public int getCount() {
        return tabNames.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabNames[position];
    }
}
