package admiral.patric.mamolapps.de.admiral.helper;

import com.unnamed.b.atv.model.TreeNode;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import admiral.patric.mamolapps.de.admiral.R;
import admiral.patric.mamolapps.de.admiral.fragments.AudioPlayerFragment;

/**
 * Created by dramalama on 13.05.2015.
 */
public class SoundTreeHolder
        extends TreeNode.BaseNodeViewHolder<SoundTreeHolder.SoundItem> {


    public SoundTreeHolder(Context context) {
        super(context);

    }

    @Override
    public View createNodeView(final TreeNode treeNode, final SoundItem soundItem) {

        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_sound_node, null, false);
        TextView tvValue = (TextView) view.findViewById(R.id.sound_name);
        ImageView showMore = (ImageView) view.findViewById(R.id.sound_showlanguage);

        tvValue.setText(soundItem.text);

        tvValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showAudioPlayerDialogFragment(soundItem);

            }
        });

        showMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (treeNode.isExpanded()) {
                    getTreeView().collapseNode(treeNode);
                } else {
                    getTreeView().expandNode(treeNode);
                }


            }
        });

        return view;
    }

    private void showAudioPlayerDialogFragment(SoundItem soundItem) {

        FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager()
                .beginTransaction();
        Fragment prev = ((FragmentActivity) context).getSupportFragmentManager()
                .findFragmentByTag("audioPlayerDialog");

        //remove previous dialogs before showing a new one
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = AudioPlayerFragment
                .newInstance(soundItem.text, soundItem.soundId);
        newFragment.show(ft, "audioPlayerDialog");

    }

    public static class SoundItem {

        public String text;

        public int soundId;


        public SoundItem(String text, int soundId) {
            this.text = text;
            this.soundId = soundId;

        }
    }
}
