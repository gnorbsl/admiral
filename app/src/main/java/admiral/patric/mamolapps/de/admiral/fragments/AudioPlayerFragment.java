package admiral.patric.mamolapps.de.admiral.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import admiral.patric.mamolapps.de.admiral.R;
import nl.changer.audiowife.AudioWife;

/**
 * Created by dramalama on 22.05.2015.
 */
public class AudioPlayerFragment extends DialogFragment {

    private static final String SOUND_ID_KEY = "soundId";

    private static final String SOUND_TITLE = "soundTitle";

    private ImageButton play;

    private View rootView;

    private ImageButton pause;

    private SeekBar seek;

    private TextView currentTime;

    private TextView totalTime;

    private ImageButton sendSound;

    public static AudioPlayerFragment newInstance(String title, int soundId) {

        AudioPlayerFragment fragment = new AudioPlayerFragment();

        Bundle arguments = new Bundle();
        arguments.putString(SOUND_TITLE, title);
        arguments.putInt(SOUND_ID_KEY, soundId);

        fragment.setArguments(arguments);

        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
            Bundle savedInstanceState) {

        getDialog().setTitle(getArguments().getString(SOUND_TITLE));

        //TODO decide if title should be visible or not
        //getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        initViews(inflater, container);

        showPlayer();

        sendSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TODO start friendlistfragment

            }
        });

        return rootView;
    }

    /**
     * Displays the AudioPlayer
     */
    private void showPlayer() {

        Uri uri = Uri
                .parse("android.resource://" + getActivity().getPackageName() + "/" + getArguments()
                        .getInt(SOUND_ID_KEY));

        AudioWife w = AudioWife.getInstance().init(getActivity(), uri)
                .setPlayView(play)
                .setSeekBar(seek)
                .setRuntimeView(currentTime)
                .setPauseView(pause)
                .setTotalTimeView(totalTime);

        w.play();
        w.pause();

    }

    /**
     * View initialization
     */
    private void initViews(LayoutInflater inflater, ViewGroup container) {

        rootView = inflater.inflate(R.layout.fragment_audio_player, container,
                false);

        play = (ImageButton) rootView.findViewById(R.id.play);
        pause = (ImageButton) rootView.findViewById(R.id.pause);
        seek = (SeekBar) rootView.findViewById(R.id.seekBar);
        currentTime = (TextView) rootView.findViewById(R.id.playback_time);
        totalTime = (TextView) rootView.findViewById(R.id.maxtime);

        sendSound = (ImageButton) rootView.findViewById(R.id.sendSound);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onPause() {
        super.onPause();
        AudioWife.getInstance().release();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
