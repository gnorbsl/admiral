package admiral.patric.mamolapps.de.admiral.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import admiral.patric.mamolapps.de.admiral.R;

public class LanguageDialogFragment extends DialogFragment implements
        AdapterView.OnItemClickListener {

    private ListView mListView;

    private String[] mLanguageOptions;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_language_dialog, container,
                false);
        getDialog().setTitle(R.string.choose_language);

        mListView = (ListView) rootView.findViewById(R.id.languageList);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mLanguageOptions = getResources().getStringArray(R.array.language_options);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, mLanguageOptions);

        mListView.setOnItemClickListener(this);
        mListView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        ((SoundsListFragment) getTargetFragment()).changeFlag(position);
        getDialog().cancel();
    }
}